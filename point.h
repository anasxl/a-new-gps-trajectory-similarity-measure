#ifndef POINT_H
#define POINT_H
#include<iostream>
#include<cmath>

using namespace std;

class point
{
public:
    
    long double x,y;
    
    point(long double _x=0, long double _y=0)
    {
        x=_x;
        y=_y;
    }
    point(const point & other)
    {
        x=other.x;
        y=other.y;
    }
    void print()
    {
        cout<<"("<<x<<", "<<y<<")"<<endl;
    }
};

inline point mid_point(point p1,point p2)
{
    return point( (p1.x+p2.x)/2, (p1.y+p2.y)/2 );
}

inline long double euclidean_distance(point p1, point p2)
{
    return sqrt( (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) );
}
inline long double dist(point p1, point p2)
{
    return euclidean_distance( p1,  p2);
}

ostream& operator<<(ostream & out, const point& p)
{
    out<<"("<<p.x<<", "<<p.y<<")";
    return out;
}
    
point operator+(point p1,point p2)
{
	point p3=p1;
	p3.x+=p2.x;
	p3.y+=p2.y;
	
	return p3;
}

#endif