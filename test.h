#include<iostream>
#include<fstream>
#include"point.h"
#include"trajectory.h"

#include"similarity.h"

using namespace std;


void test_our()
{
    cout<<"*********************************************"<<endl;
    cout<<"Testing our algorithm: Start"<<endl;
    cout<<"***"<<endl;
    int i;
    
    trajectory t1("1","2");


    trajectory t2=t1;
    trajectory t3("1","2");
   
    for(i=0;i<4;i++)
    {
        t1.insert_back(point(2*i,0));
        t3.insert_back(point(2*i,0));

        t2.insert_back(point(4*i,0));
    }

    t3.insert_at(2,point(3,1));
    t3.insert_back(point(6,1));

    cout<<"t1: ";
    t1.print();
    cout<<"t2: ";
    t2.print();
    cout<<"t3: ";
    t3.print();
    cout<<"distance between t1 and t1: "<<our_distance(t1,t1)<<endl;
    cout<<"distance between t1 and t2: "<<our_distance(t1,t2)<<endl;
    cout<<"distance between t2 and t2: "<<our_distance(t2,t2)<<endl;
    cout<<"distance between t1 and t3: "<<our_distance(t1,t3)<<endl;

    
    
    cout<<"***"<<endl;
    cout<<"Testing our algorithm: End"<<endl;
    cout<<"*********************************************"<<endl;
}


void test()
{

    test_our();
	
}
