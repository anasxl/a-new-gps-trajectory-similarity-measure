This is the implementation of a new similarity measure for spatial trajectory data. The algorithm can be found in this paper "A New Trajectory Similarity Measure for GPS Data. Anas Ismail and Antoine Vigneron. In Proc. 6th ACM SIGSPATIAL International Workshop on GeoStreaming (IWGS 2015)."
https://algo.kaust.edu.sa/Documents/manuscripts/trajectory.pdf

##
The algorithm is described in the paper with some experiments. 

##
The code provided here is only for the core algorithm. No code for the experiments, mentioned in paper, is provided.


##