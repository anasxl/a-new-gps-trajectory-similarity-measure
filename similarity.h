#include"trajectory.h"
#include"point.h"
#include <limits>
#include<cmath>
using namespace std;



void print( vector< vector<long double> > & array)//Print a 2D array
{
    int m=array.size();
    int n=array[0].size();
    
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            cout<<array[i][j]<<" ";
        }
        cout<<endl;
    }
}


long double our(trajectory & t1, trajectory & t2)
{
    long double inf = numeric_limits<long double>::infinity();
  
    int n=t1.length();
    int m=t2.length();

    
    int i,j;
    vector< vector<long double> > A(m+1, vector<long double>(n+1,inf)) ;//ending at point in t1
    vector< vector<long double> > B(m+1, vector<long double>(n+1,inf)) ;//ending at point in t2
    
    ///
    /// t1 and t2 are zero based.
    ///
    /// trajectory 1 is of length n, with index i
    /// trajectory 2 is of length m, with index j
    /// A[j][i] represents the cost of merging trajectory1 from start to point i and trajectory2 from start to point j, ending at point i.
    ///B[j][i] same but ending at point j.
    ///
    ///you can think of the tables A and B as if t1 is horizontal and t2 is vertical
    
    //intializations
    A[0][0]=0;
    A[0][1]=0;
    for(i=2;i<=n;i++)
    {
        /*B[0][i]=*/A[0][i]=A[0][i-1]+euclidean_distance(t1.at(i-1),t1.at(i-2));
    }
    B[0][0]=0;
    B[1][0]=0;
    for(j=2;j<=m;j++)
    {
        /*A[j][0]=*/B[j][0]=B[j-1][0]+euclidean_distance(t2.at(j-1),t2.at(j-2));
    }
    
    
    long double a1,a2,b1,b2,b3;

    for(j=1;j<=m;j++)
    {
        for(i=1;i<=n;i++)
        {
            //if j==1, this means there is only one point in t2 which means we can't access the previous point in t2. similar thing will happen when i==1
            
            
            a1=B[j][i-1]+euclidean_distance(t1.at(i-1),t2.at(j-1));
            a2=a1;
            if(i>1)
                a2=A[j][i-1]+euclidean_distance(t1.at(i-1),t1.at(i-2));
            A[j][i]=min(a1,a2);

            b1=A[j-1][i]+euclidean_distance(t2.at(j-1),t1.at(i-1));
            b2=b1;
            if(j>1)
                b2=B[j-1][i]+euclidean_distance(t2.at(j-1),t2.at(j-2));
            B[j][i]=min(b1,b2);
			
            /*
			if(A[j][i]<0 || B[j][i]<0)
			{
				cout<<"Overflow happens"<<endl;
			}
            */
            
        }
    }
    
    return min(A[m][n],B[m][n]);
    
}
long double our_distance(trajectory & t1, trajectory & t2, bool remove=false,long double threshold=0.0)//Normalized distance. 
{
	long double distance=our(t1, t2);
    long double average=((t1.norm()+t2.norm())/2.0);
	return log( distance/average ) ;	
}
