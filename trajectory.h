#ifndef TRAJECTORY_H
#define TRAJECTORY_H
#include<iostream>
#include<cmath>
#include<vector>
#include<string>
#include<cstdlib>
#include"point.h"

using namespace std;

class trajectory
{
    int size;
    vector<point> traj;
public:
    string t_id;
    string label;//Which class it belongs to
    
    trajectory(string _id="", string _label="")
    {
        t_id=_id;
        label=_label;
        size=0;
    }
	long double norm()//The length of the trajectory. Used for normalization.
	{
		long double d=0;
		for (int i=1;i<size;i++)
		{
			d+=euclidean_distance(traj[i],traj[i-1]);
		}		
		return d;
	}
    void insert_back(point p)
    {
        traj.push_back(p);
        size++;
    }
    int length()//the number of points in a trajectory
    {
        return size;
    }
    bool insert_at(int pos, point p)
    {
        if(pos<0 || pos>size)
        {
             cout<<"Can't insert element at position: "<<pos<<". Size is:"<<size<<". You can only insert values in the range [0,"<<size<<"]."<<endl;
            return false;
        }
        traj.insert(pos+traj.begin(), p);
        
        size++;
        return true;
    }
	bool add_noise_at(int pos, point p)
    {
        if(pos<0 || pos>size)
        {
			cout<<"Can't add noise at position: "<<pos<<". Size is:"<<size<<". You can only insert values in the range [0,"<<size<<"]."<<endl;
            return false;
        }
		traj[pos]=traj[pos]+p;//add noise
        return true;
    }
	
    bool remove_at(int pos)
    {
        if(size==0)
        {
            cout<<"Size is: 0. You can't remove any element."<<endl;
            return false;
        }
        if(pos<0 || pos>size)
        {
            cout<<"Can't remove element at position: "<<pos<<". Size is:"<<size<<". You can only remove elements in the range [0,"<<size-1<<"]."<<endl;
            return false;
        }
        traj.erase (pos+traj.begin());
        
        size--;
        return true;
    }
    point& at(int pos)
    {
        //point p(-123456,-123456);
        if(pos<0 || pos>=size)
        {
            cout<<"Exiting: No element at position: "<<pos<<". Size is:"<<size<<endl;
            exit(1);
            //return p;
        }
        return traj[pos];
    }
    void print()
    {
    
        cout<<"<ID:"<<t_id<<", Label:'"<<label<<"'>("<<size<<")[";
        for(int i=0;i<size-1;i++)
            cout<<traj[i]<<", ";
        
        if(size>0)
            cout<<traj[size-1];
        cout<<"]"<<endl;
    }
    ~trajectory()
    {
        //delete traj;
    }

    
    
};

#endif
